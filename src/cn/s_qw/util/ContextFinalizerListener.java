package cn.s_qw.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.mysql.cj.jdbc.AbandonedConnectionCleanupThread;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;

/**
 * 解决内存泄漏，线程堆栈问题
 * 设置监听器，调用方法
 */


@WebListener()
public class ContextFinalizerListener implements ServletContextListener {

    public ContextFinalizerListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        /* This method is called when the servlet context is initialized(when the Web application is deployed). */
        //在初始化servlet上下文时(在部署Web应用程序时)调用此方法。
        System.out.println("web项目已启动");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        /* This method is called when the servlet Context is undeployed or Application Server shuts down. */
        //当取消部署servlet上下文或Application Server关闭时，将调用此方法
        //注销驱动
        DruidDataSource druidDataSource= (DruidDataSource) JDBCUtils.getDataSource();
        druidDataSource.close();

        System.out.println("关闭DruidDataSource连接池对象");

        try {
            Enumeration<Driver> drivers = DriverManager.getDrivers();
            while(drivers.hasMoreElements()) {
                DriverManager.deregisterDriver(drivers.nextElement());
                System.out.println("解除注册");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //关闭线程
        try{
            AbandonedConnectionCleanupThread.checkedShutdown();
            System.out.println("关闭 Abandoned 线程");
        }catch (Exception e){
            System.out.println("ContextFinalizer:SEVERE problem cleaning up: " + e.getMessage());
            e.printStackTrace();
        }



    }
}

