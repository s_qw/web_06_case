package cn.s_qw.web.servlet;

import cn.s_qw.domain.PageBean;
import cn.s_qw.domain.User;
import cn.s_qw.service.UserService;
import cn.s_qw.service.impl.UserServiceImpl;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.Map;

@WebServlet("/findUserByPageServlet")
public class FindUserByPageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf-8");

        //1.获取参数
        String currentPage = request.getParameter("currentPage");//当前页码
        String rows = request.getParameter("rows");//每页显示条目数

        //避免获取不到参数时，空指针异常
        if (currentPage == null || "".equals(currentPage)) {
            currentPage = "1";
        }

        if (rows == null || "".equals(rows)) {
            rows = "5";
        }

        //获取条件查询的参数 (condition size = 0:未接收到参数)
        Map<String,String[]> condition = request.getParameterMap();



        //2.调用service查询
        UserService service = new UserServiceImpl();
        PageBean<User> pb = service.findUserByPage(currentPage,rows,condition);

        //控制台打印PageBean{}，测试
        //System.out.println(pb);

        //3.将PageBean存入request("pb",pb:键名，值)
        request.setAttribute("pb",pb);
        request.setAttribute("condition",condition);//将查询条件存入request，信息回显

        //4.转发到list.jsp：转发不需要虚拟目录
        request.getRequestDispatcher("/list.jsp").forward(request,response);

    }
}
