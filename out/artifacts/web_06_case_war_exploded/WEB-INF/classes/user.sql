/*
Navicat MySQL Data Transfer

Source Server         : localhost_qw
Source Server Version : 80017
Source Host           : localhost:3306
Source Database       : web_6

Target Server Type    : MYSQL
Target Server Version : 80017
File Encoding         : 65001

Date: 2022-04-26 13:48:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gender` varchar(5) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(32) DEFAULT NULL,
  `qq` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('2', '小芳', '女', '22', '广东', '123456789', 'xiaofang@qq.com');
INSERT INTO `user` VALUES ('3', '李四', '女', '30', '广东', '123456789', '123456789@qq.com');
